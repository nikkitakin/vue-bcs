import Vue from 'vue';
import PerfectScrollbar from 'vue2-perfect-scrollbar';
import router from './router';
import store from './store';
import App from './App.vue';

// Stylesheet
import './assets/scss/main.scss';
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css';

Vue.use(PerfectScrollbar);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
