import Vue from 'vue';
import Vuex from 'vuex';

// Filters
import VueCurrencyFilter from 'vue-currency-filter';

// Modules
import investmentOptions from './modules/investmentOptions';

Vue.use(Vuex);

// Filters
Vue.use(VueCurrencyFilter, {
  symbol: '₽',
  thousandsSeparator: ' ',
  fractionCount: 0,
  fractionSeparator: '',
  symbolPosition: 'back',
  symbolSpacing: true,
  avoidEmptyDecimals: undefined,
});

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    investmentOptions,
  },
});
