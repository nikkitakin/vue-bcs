// import Vue from 'vue';

export default {
  state: () => ({
    investmentAmount: 70000,
    period: {
      dateStart: new Date(new Date().setDate(1)),
      dateEnd: new Date(new Date().setFullYear(new Date().getFullYear() + 5)),
      years: 5,
      months: 0,
    },
    cashIn: [
      /* {
        transactionId: 0,
        date: new Date(2022, 6, 15),
        amount: 50000,
      },

      {
        transactionId: 0,
        date: new Date(2024, 3, 15),
        amount: 20000,
      }, */
    ],
    cashOut: [
    ],
  }),

  actions: {
    setInvestmentPeriod({ commit, state }, payload) {
      const dateStart = new Date(state.period.dateStart);
      const startYear = dateStart.getFullYear();
      const startMonth = dateStart.getMonth();
      let dateEnd = dateStart;
      dateEnd.setFullYear(startYear + Number(payload.years));
      dateEnd.setMonth(startMonth + Number(payload.months));
      dateEnd = new Date(dateEnd);
      commit('investmentYears', payload.years);
      commit('investmentMonths', payload.months);
      commit('investmentPeriodEnd', dateEnd);
    },

    updateCashInTransaction({ state }, payload) {
      console.log(payload);
      const transactions = state.cashIn;
      transactions.find(
        (element) => element.transactionId === payload.transactionId,
      ).amount = payload.amount;
    },
  },

  mutations: {
    investmentAmount(state, payload) {
      state.investmentAmount = Number(payload);
    },

    investmentYears(state, payload) {
      state.period.years = payload;
    },

    investmentMonths(state, payload) {
      state.period.months = payload;
    },

    investmentPeriodEnd(state, payload) {
      console.log(payload);
      state.period.dateEnd = payload;
    },
  },
};
